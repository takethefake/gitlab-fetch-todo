FROM node:13
WORKDIR /var/app

ADD package.json .
ADD yarn.lock .
RUN yarn install
ADD . .
RUN yarn build
CMD yarn start