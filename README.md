# Gitlab Notifcations Service for Slack

This service notifys when ever you get a new notification in gitlab and send you a message via
webhook in Slack. 

# Setup

- Open the Workflow Builder in Slack.
- Create a new Workflow and choose webhook to start it
- Add the following variables
    - user:string
    - target_url:string
    - target_type:string
    - target_name:string
- add 'send message' as a next step where you select yourself as the recipient
- copy the webhook url (SLACK_WEBHOOK later in env)
- go to gitlab settings
- create a new access token with rights to `read_api` (PERSONAL_GITLAB_TOKEN later in env)
- `mv .env.dist .env`
- adjust `.env` to match your environment 
- `yarn install`
- now you could start the service with `yarn dev` or `yarn build && yarn start`

# Daemonize service

if your system uses systemd i've provided a service unit which can be installed

- sudo cp gitlab-fetch-todo.service /etc/systemd/system/ 
- systemctl daemon-reload
- systemctl enable gitlab-fetch-todo
- systemctl start gitlab-fetch-todo

