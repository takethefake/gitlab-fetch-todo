export interface IndexedGitlabTodo {
  [id: string]: GitlabTodo;
}

export interface GitlabTodo {
  id: number;
  project: Project;
  author: Author;
  action_name: string;
  target_type: string;
  target: Target;
  target_url: string;
  body: string;
  state: string;
  created_at: Date;
  updated_at: Date;
}

export interface Author {
  id: number;
  name: string;
  username: string;
  state: string;
  avatar_url: string;
  web_url: string;
}

export interface Project {
  id: number;
  description: string;
  name: string;
  name_with_namespace: string;
  path: string;
  path_with_namespace: string;
  created_at: Date;
}

export interface Target {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  created_at: Date;
  updated_at: Date;
  merged_by: Author;
  merged_at: Date;
  closed_by: null;
  closed_at: null;
  target_branch: string;
  source_branch: string;
  user_notes_count: number;
  upvotes: number;
  downvotes: number;
  assignee: null;
  author: Author;
  assignees: any[];
  source_project_id: number;
  target_project_id: number;
  labels: any[];
  work_in_progress: boolean;
  milestone: null;
  merge_when_pipeline_succeeds: boolean;
  merge_status: string;
  sha: string;
  merge_commit_sha: string;
  squash_commit_sha: null;
  discussion_locked: null;
  should_remove_source_branch: null;
  force_remove_source_branch: boolean;
  reference: string;
  references: References;
  web_url: string;
  time_stats: TimeStats;
  squash: boolean;
  task_completion_status: TaskCompletionStatus;
  has_conflicts: boolean;
  blocking_discussions_resolved: boolean;
  approvals_before_merge: null;
  subscribed: boolean;
  changes_count: string;
  diff_refs: DiffRefs;
  merge_error: null;
  user: User;
}

export interface DiffRefs {
  base_sha: string;
  head_sha: string;
  start_sha: string;
}

export interface References {
  short: string;
  relative: string;
  full: string;
}

export interface TaskCompletionStatus {
  count: number;
  completed_count: number;
}

export interface TimeStats {
  time_estimate: number;
  total_time_spent: number;
  human_time_estimate: null;
  human_total_time_spent: null;
}

export interface User {
  can_merge: boolean;
}
