import { GitlabTodo, IndexedGitlabTodo } from "./types";
require("dotenv").config();
import fetch from "node-fetch";
import fs from "fs";
const saveFilePath = `${__dirname}/../data/submitted.json`;

let abort = false;
if (!process.env.GITLAB_TODO_URL) {
  console.log("missing ENV GITLAB_TODO_URL");
  abort = true;
}

if (!process.env.PERSONAL_GITLAB_TOKEN) {
  console.log("missing ENV PERSONAL_GITLAB_TOKEN");
  abort = true;
}

if (!process.env.SLACK_WEBHOOK) {
  console.log("missing ENV SLACK_WEBHOOK");
  abort = true;
}
if (abort) {
  process.exit();
}

const reduceGitlabTodoToIndex = (data: GitlabTodo[]): IndexedGitlabTodo => {
  return data.reduce<IndexedGitlabTodo>((prev, cur) => {
    prev[cur.id] = cur;
    return prev;
  }, {});
};

const fetchData = async (): Promise<IndexedGitlabTodo> => {
  try {
    const data = await fetch(process.env.GITLAB_TODO_URL!, {
      headers: { "PRIVATE-TOKEN": process.env.PERSONAL_GITLAB_TOKEN! },
    });
    const dataJSON = (await data.json()) as GitlabTodo[];

    return reduceGitlabTodoToIndex(dataJSON);
  } catch (e) {
    console.log(e);
    return {};
  }
};

const loadSubmittedData = (): IndexedGitlabTodo => {
  try {
    if (!fs.existsSync(saveFilePath)) {
      fs.writeFileSync(saveFilePath, JSON.stringify({}));
    }
    const data = fs.readFileSync(saveFilePath);
    return JSON.parse(data.toString()) as IndexedGitlabTodo;
  } catch (e) {
    console.log("could not load initial submitted data", e);
  }
  return {};
};

const saveSubmittedData = (data: IndexedGitlabTodo) => {
  fs.writeFileSync(saveFilePath, JSON.stringify(data));
};

const getUnsubmittedData = (
  newData: IndexedGitlabTodo,
  oldData: IndexedGitlabTodo
) => {
  return Object.keys(newData).reduce<IndexedGitlabTodo>((prev, cur) => {
    if (oldData[cur] === undefined) {
      prev[cur] = newData[cur];
    }
    return prev;
  }, {});
};

const submitData = async (data: IndexedGitlabTodo) => {
  console.log(data);
  const formattedData = Object.values(data).map((todo) => {
    return {
      user: todo.author.name,
      target_url: todo.target_url,
      action_name: todo.action_name,
      target_type: todo.target_type,
    };
  });
  await Promise.all(
    formattedData.map((todo) => {
      return fetch(process.env.SLACK_WEBHOOK!, {
        body: JSON.stringify(todo),
        method: "POST",
        headers: { "content-type": "application/json" },
      });
    })
  );
};

const main = async () => {
  const newData = await fetchData();
  const unsubmittedData = getUnsubmittedData(newData, oldData);
  submitData(unsubmittedData);
  oldData = { ...oldData, ...newData };
  saveSubmittedData(oldData);
  console.log("timeout");
  setTimeout(main, 20000);
};

let oldData = loadSubmittedData();
main();
